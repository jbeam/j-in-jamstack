/**
 * @file Eleventy’s configuration file
 * @author Reuben L. Lillie
 * See {@link https://www.11ty.io/docs/config/ 11ty docs}
 */

// Require Node.js native fs module for interacting with the file system
var fs = require('fs')
// https://github.com/jakubpawlowicz/clean-css
var CleanCSS = require('clean-css')
// https://github.com/kangax/html-minifier
var htmlmin = require("html-minifier")
// https://github.com/terser-js/terser
var Terser = require('terser')

/** @module .eleventy */
module.exports = function (eleventyConfig) {

  /**
   * Define a collection for all the slideshow slides
   * @param {Object} 11ty’s collection object
   * @return {Array} Data based on all markdown files in src/slides/
   * See {@link https://www.11ty.io/docs/collections/#getfilteredbyglob(-glob-) 11ty docs}
   */
  eleventyConfig.addCollection('slides', function (collection) {
    return collection.getFilteredByGlob('src/slides/*.md')
      // Sort them by file name (i.e., slide order) instead of by date
      .sort(function (a, b) {
				return a.data.page.fileSlug - b.data.page.fileSlug
      })
  })

  /**
   * Convert a file’s contents to a single string
	 * @example `${this.fileToString('/includes/assets/css/inline.css')}`
   * @param {String} file The path to the file
   * @return {String} A stringified version of the file’s contents
   * See {@link https://www.11ty.io/docs/filters/ 11ty docs}
   */
  eleventyConfig.addFilter('fileToString', function (file) {
		return fs.readFileSync(`src/${file}`).toString()
	})

  /**
   * Minify inline CSS
   * See {@link https://www.11ty.io/docs/quicktips/inline-css/ 11ty docs}
   */
  eleventyConfig.addFilter('minifyCSS', function(stylesheet) {
    return new CleanCSS({}).minify(stylesheet).styles
  })

  /**
   * Minify JavaScript
   * See {@link https://www.11ty.io/docs/quicktips/inline-js/ 11ty docs}
   */
    eleventyConfig.addFilter("minifyJS", function(code) {
      var minified = Terser.minify(code)
      if(minified.error) {
        console.log("Terser error: ", minified.error)
        return code
      }

      return minified.code
    })

  /**
   * Minify HTML
   * See {@link https://www.11ty.io/docs/config/#transforms 11ty docs}
   */
  eleventyConfig.addTransform("minifyHTML", function(content, outputPath) {
    if( outputPath.endsWith(".html") ) {
      var minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      })
      return minified
    }

    return content
  })

  /**
   * A link to edit this page with the remote Git repository
   * @method
   * @name editThisPage
   * @param {Object} data The 11ty `data` object
   * @param {String} editLinkText Alternate link text (optional)
   * @return {String} HTML template literal
   * @example `${this.editThisPage(data, 'Improve this page')}`
   * See {@link https://www.11ty.io/docs/quicktips/edit-on-github-links/ 11ty docs}
   */
  eleventyConfig.addShortcode('editThisPage', function (data, editLinkText) {
    return `
      <a class="edit-link" href="${data.pkg.repository.url}/tree/master/${data.page.inputPath}">
        ${editLinkText && typeof editLinkText === 'string'
          ? `${editLinkText}`
          : 'Edit this page'
        }
      </a>
    `
  })
  /**
   * A shortcode for applying the proper favicon
	 * @example `${this.favicon}`
	 * @return {String} A composite HTML template
   * See {@link https://css-tricks.com/favicon-quiz/ CSS Tricks}
   * See {@link https://realfavicongenerator.net/ Favicon Generator}
   * See {@link https://www.11ty.io/docs/shortcodes/ 11ty docs}
   */
  eleventyConfig.addShortcode('favicon', function () {
		return `
			<link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
			<link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
			<link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
			<link rel="manifest" href="/favicons/site.webmanifest">
			<link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#101820">
			<link rel="shortcut icon" href="/favicons/favicon.ico">
			<meta name="msapplication-TileColor" content="#ffb612">
			<meta name="msapplication-config" content="/favicons/browserconfig.xml">
			<meta name="theme-color" content="#101820">`
	})

  /**
   * A shortcode to load all the slides for the slideshow layout
	 * @example `${this.slideshow(data)}`
	 * @param {Object} data The 11ty `data` object
	 * @return {String} A composite HTML template
   * See {@link https://www.11ty.io/docs/shortcodes/ 11ty docs}
   */
  eleventyConfig.addShortcode('slideshow', function (data) {
    return `${data.collections.slides.map(function (slide) {
      return `
        <section id="${data.collections.slides.indexOf(slide)}" class="grid slide ${slide.data.classList ? slide.data.classList.map(c => c).join(' ') : ''}" data-slide-type="${slide.data.slideType}">
          ${slide.data.title
            ? `<header class="justify-content-center text-center">
              <h2>${slide.data.title}</h2>
            </header>`
            : ''
          }
          <div class="content align-content-start">
            ${slide.templateContent}
          </div>
          <footer class="small gray edit-link">
            <a href="${data.pkg.repository.url}/tree/master/${slide.data.page.inputPath}">
              Edit this slide
            </a>
          </footer>
        </section>
    `}).join('')}`
  })

  /**
	 * OpenGraph and Twitter metadata with fallbacks
	 * @example `${this.socialMeta(data)}`
	 * @param {Object} data The 11ty `data` object
	 * @return {String} A composite HTML template
	 * See {@link https://css-tricks.com/essential-meta-tags-social-media/ Adam Coti, “The Essential Meta Tags for Social Media,” _CSS-Tricks_ (updated December 21, 2016)}
	 */
	eleventyConfig.addShortcode('socialMeta', function (data) {
		var html = ''

    data.title
			? html += `
				<meta property="og:title" content="${data.title}">
				<meta name="twitter:title" content="${data.title}">
			`
			: html += `
				<meta property="og:title" content="${data.site.title}">
				<meta name="twitter:title" content="${data.site.title}">
			`
		data.description
			? html += `
				<meta property="og:description" content="${data.description}">
				<meta property="twitter:description" content="${data.description}">
			`
			: html += `
				<meta property="og:description" content="${data.pkg.description}">
				<meta property="twitter:description" content="${data.pkg.description}">
			`
		data.thumbnail
			? html += `
				<meta property="og:image" content="/assets/img/${data.thumbnail}">
				<meta name="twitter:image" content="/assets/img/${data.thumbnail}">
				<meta name="twitter:card" content="summary_large_image">
			`
			: html += `
				<meta property="og:image" content="/favicons/android-chrome-512x512.png">
				<meta name="twitter:image" content="/favicons/android-chrome-512x512.png">
				<meta name="twitter:card" content="summary_large_image">
			`
		html += `<meta property="og:url" content="${data.page.url}">`

    return html

  })

  /**
   * Combine arrays for 11ty data objects, instead of replacing them
   * See {@link https://www.11ty.io/docs/config/#data-deep-merge 11ty docs}
   */
  eleventyConfig.setDataDeepMerge(true)

  /**
   * Copy static assets from the input directory to the output directory
   * See {@link https://www.11ty.io/docs/copy/ 11ty docs}
   */
  eleventyConfig.addPassthroughCopy('src/favicons')
  eleventyConfig.addPassthroughCopy('src/includes/assets')

  /**
   * Return Eleventy’s optional Config object
   * See {@link https://www.11ty.io/docs/config/#using-the-configuration-api 11ty docs}
   */
  return {
    dir: {
      data: 'data', // relative to input
      includes: 'includes', // relative to input
      input: 'src',
      output: 'dist'
    }
  }

}
