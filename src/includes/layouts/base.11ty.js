/**
 * @file Defines the base layout for wrapping other template layouts
 * @author Reuben L. Lillie
 */

/**
 * The JavaScript template for the base layout
 * @module src/includes/layouts/base
 * @param {Object} data 11ty’s data object
 * @return {String} The HTML template
 * See {@link https://www.11ty.io/docs/languages/javascript/ 11ty docs}
 */
module.exports = function (data) {
  return `
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${data.title ? data.title : data.site.title}</title>
    ${this.favicon()}
    <meta charset="utf-8">
    <meta name="viewport" content="width:device-width,initial-scale=1.0">
    ${this.socialMeta(data)}
    <style>
      ${this.minifyCSS(`
        :root {
          --background-color: ${data.master.background.color};
          --base-unit: ${data.master.baseUnit};
        }
        body {
          background-image: url(${data.master.background.image}_small.jpg);
        }
        @media screen and (min-width: 641px), screen and (min-height: 424px) {
          body {
            background-image: url(${data.master.background.image}_medium.jpg);
          }
        }
        @media screen and (min-width: 1281px), screen and (min-height: 848px) {
          body {
            background-image: url(${data.master.background.image}_large.jpg);
          }
        }
        @media screen and (min-width: 1921px), screen and (min-height: 1272px) {
          body {
            background-image: url(${data.master.background.image}_x-large.jpg);
          }
        }
      `)}
      ${this.minifyCSS(this.fileToString('/includes/assets/css/fonts.css'))}
      ${this.minifyCSS(this.fileToString('/includes/assets/css/inline.css'))}
    </style>
  </head>
  <body class="${data.page.url === '/' ? 'presentation' : ''}">
    ${data.content}
  </body>
</html>`
}
