/**
 * @file The nested content template for basic pages
 * @author Reuben L. Lillie
 * See {@link https://www.11ty.io/docs/languages/javascript/#optional-data-method 11ty docs}
 */

/**
 * Creates a new Content template
 * @class
 */
class Content {

  data() {
    return {
      layout: 'layouts/base'
    }
  }

  render(data) {
    return `
    <main id="content">
      <article class="grid margin padding background border-radius">
        <header class="text-center">
          <h2>${data.title}</h2>
        </header>
        ${data.content}
        <footer class="small gray edit-link">
          ${this.editThisPage(data)}
         </footer>
      </article>
      <footer id="main_footer" class="small gray">
        <a href="/">Return to slideshow</a>
      </footer>
    </main>
    `
  }

}

module.exports = Content
