/**
 * @file The nested references template
 * @author Reuben L. Lillie
 * See {@link https://www.11ty.io/docs/languages/javascript/#optional-data-method 11ty docs}
 */

/**
 * Creates a new References template
 * @class
 */
class References {

  data() {
    return {
      layout: 'layouts/content'
    }
  }

  render(data) {
    return `
      <ul class="no-list-style hanging-indent">
        ${data.references.map(function (reference) {
          return `<li>${reference}</li>`
        }).join('')
        }
      </ul>
    `
  }

}

module.exports = References
