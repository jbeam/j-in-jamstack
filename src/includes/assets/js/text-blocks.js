/**
 * @file Size fonts for text blocks to be full-width and “justified”
 * @author Reuben L. Lillie
 * See {@link https://codepen.io/reubenlillie/post/full-width-text-blocks}
 */
(function () {

  'use strict'

  // Compiles a node list of indiviual lines inside text blocks
  var lines = document.querySelectorAll('.text-block *')

  /**
   * Wraps each character of a string inside a <span> element
   * @param {NodeList} lines
   * @return {String} each character wrapped in a <span>
   */
  var wrapCharacters = function (lines) {
    return lines.forEach(function (line) {
      var characters = line.innerHTML.split(['','>','<']);
      var wrappedCharacters = characters
        .map(function (character) {
          // Convert spaces to non-breaking spaces
          if (character === ' ') {
            return '<span class="text-line">&nbsp;</span>'
          }
          // Prevent < and > from being escaped
          if (character === '>' || character === '<') {
            return '<span class="text-line">' + character + '</span>'
          }
          // Wrap all other characters
          return '<span class="text-line">' + character + "</span>"
        })
        .join("");
      return (line.innerHTML = wrappedCharacters)
    })
  }

  /**
   * Measures the length of each line and sets its font size accordingly
   * @param {NodeList} elems A group of elements (e.g., lines)
   * @return {String|Function}
   */
  var setFontSize = function (elems) {
    return elems.forEach(function (elem) {
      // Only adjust font size for lines more than one character long
      return (elem.textContent.length > 1)
        ? elem.style.fontSize = 150 / elem.innerHTML.length + 'vmin'
        : ''
    })
  }

  /**
   * First set the font size for text blocks
   * Then wraps the characers in those lines
   */
  var formatTextBlocks = function() {
    setFontSize(lines)
    wrapCharacters(lines)
  }

  formatTextBlocks()

})()
