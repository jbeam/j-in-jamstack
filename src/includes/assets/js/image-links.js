/**
 * @file Wrap images in anchor tags
 * @author Reuben L. Lillie
 */
(function () {

  'use strict'

  // Compiles a node list of all <img> elements on a page
  var imageNodes = document.getElementsByTagName('img')
  var images = Array.prototype.slice.call(imageNodes)

  /**
   * Wraps <img> elements in <a> tags linking to the <img> src attribute
   * @param {Array} arr An array of images
   * @return {String} HTML for each image in the array
   */
  var anchorImages = function (arr) {
    return arr.map(function (img) {
      var newHTML
      if(img.parentNode.nodeName === 'A' || img.closest('button')) return
      // Check if img has a src attribute and is not already wrapped
      img.hasAttribute('src')
        ? newHTML = '<a href="' + img.getAttribute('src')  + '"' +
          'target="_blank">' +
          img.outerHTML +
          '</a>'
        : newHTML = img.parentNode.innerHTML
      return img.outerHTML = newHTML
    }).join ('')
  }

  // Add links for all images on the page
  anchorImages(images)

})()
